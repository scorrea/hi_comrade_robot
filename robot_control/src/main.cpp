#include <Arduino.h>
#include <Servo.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 6, 5, 4, 3);

// Servo of claw, base, link1 and link2
// Link1 is the longest link of the robot arm
Servo claw, base, link1, link2;

// Degree position of servos
bool claw_open = false;
int base_pos = 90;
int link1_pos = 90;
int link2_pos = 90;

// Perimeter in which the nearest joint to the arm is inside
String zone = "0"; // From 0 to 3, the closest zone to the arm is the 0
                   // If there aren't any joint, the zone is 3

// Consts to define the servo to use
const byte servo_claw = 1;
const byte servo_base = 2;
const byte servo_link1 = 3;
const byte servo_link2 = 4;

// Consts to define the maximum and minium position in degrees of servos
const int max_base = 60;
const int min_base = 30;
const int max_link1 = 95;
const int min_link1 = 140;
const int max_link2 = 85;
const int min_link2 = 60;
bool max_pos = true;

int state = 0;
unsigned long men;

// Function for controlling servos
void pos_control(int pos_f, int pos_a, int inc, byte servo)
{
  //To control angular velocity, make increments between actual angel and the next until it reachs the desired
  //pos_f is the target position, pos_a is the actual

  if (zone == "0")
  {
    pos_a = pos_f;
  }

  while (pos_a != pos_f)
  {
    if (pos_a < pos_f)
    {
      pos_a = pos_a + inc;

      if (pos_a > pos_f)
      { //If we pass the desire value...
        pos_a = pos_f;
      }
    }

    if (pos_a > pos_f)
    { //Objective position is less than actual
      pos_a = pos_a - inc;

      if (pos_a < pos_f)
      { //If we pass the desire value...
        pos_a = pos_f;
      }
    }

    switch (servo)
    {
    case 1: //Claw servo
      claw.write(pos_a);
      break;

    case 2: //Base servo
      base.write(pos_a);
      base_pos = pos_a;
      break;

    case 3: //Link1 servo
      link1.write(pos_a);
      link1_pos = pos_a;
      break;

    case 4: //Link2 servo
      link2.write(pos_a);
      link2_pos = pos_a;
      break;
    }
    delay(10);
  }
}

// Speed control function
int zone_joint(String zone)
{
  //In function of the ubication among zones of the nearest joint to robot, select the speed
  int inc_vel = 0;

  if (zone == "0")
  {
    inc_vel = 0;
  }
  else
  {

    if (zone == "1")
    {
      inc_vel = 1;
    }

    else
    {
      if (zone == "2")
      {
        inc_vel = 2;
      }

      else
      {
        if (zone == "3")
        {
          inc_vel = 4; //4 or 5
        }

        else
        {
          inc_vel = 0;
        }
      }
    }
  }
  return (inc_vel);
}

// Open claw function
void open_claw(String zone)
{
  if (claw_open == false)
  {
    int inc = zone_joint(zone);
    const int claw_pos_f = 150;
    //NOTE: The claw is or not open -> OPEN = 39 degrees
    pos_control(claw_pos_f, 50, inc, servo_claw);
    claw_open = true;
  }
}

// Close claw function
void close_claw(String zone)
{
  if (claw_open == true)
  {
    int inc = zone_joint(zone);
    const int claw_pos_f = 50;
    //NOTE: The claw is or not open -> OPEN = 150 degrees
    pos_control(claw_pos_f, 150, inc, servo_claw);
    claw_open = false;
  }
}

// Move servo of link 1
void move_link1(String zone, int pos_f)
{
  int inc = zone_joint(zone);
  pos_control(pos_f, link1_pos, inc, servo_link1);
}

// Move servo of link 2
void move_link2(String zone, int pos_f)
{
  int inc = zone_joint(zone);
  pos_control(pos_f, link2_pos, inc, servo_link2);
}

// Move servo of robot base
void move_base(String zone, int pos_f)
{
  int inc = zone_joint(zone);
  pos_control(pos_f, base_pos, inc, servo_base);
}

// Move arm to home
void go_home()
{
  open_claw("2");
  delay(250);
  move_base("2", 45);
  delay(250);
  move_link1("2", 95);
  delay(250);
  move_link2("2", 85);
  delay(250);
}

// Sequence to teste and measure servo speed
void servo_test_speed(String zone, int servo, bool pos)
{

  switch (servo)
  {
  case 2:
    if (pos != true)
    {
      move_base(zone, max_base);
    }
    else
    {
      move_base(zone, min_base);
    }
    break;

  case 3:
    if (pos != true)
    {
      move_link1(zone, max_link1);
    }
    else
    {
      move_link1(zone, min_link1);
    }
    break;

  case 4:
    if (pos != true)
    {
      move_link2(zone, max_link2);
    }
    else
    {
      move_link2(zone, min_link2);
    }
    break;
  }
}

// State machine to sequence control of arm positions
void state_machine(String zone, int state)
{

  switch (state)
  {
  case 1:
    move_link1(zone, 100);
    move_link2(zone, 80);
    break;

  case 2:
    move_link1(zone, 120);
    move_link2(zone, 70);
    break;

  case 3:
    move_link1(zone, 140);
    move_link2(zone, 60);
    close_claw(zone);
    break;

  case 4:
    move_link1(zone, 120);
    move_link2(zone, 70);
    break;

  case 5:
    move_base(zone, 45);
    break;

  case 6:
    move_link1(zone, 95);
    move_link2(zone, 85);
    break;

  case 7:
    move_base(zone, 30);
    break;

  case 8:
    move_link1(zone, 120);
    move_link2(zone, 70);
    break;

  case 9:
    move_link1(zone, 140);
    move_link2(zone, 60);
    open_claw(zone);
    break;

  case 10:
    move_link1(zone, 120);
    move_link2(zone, 70);
    break;

  case 11:
    move_base(zone, 45);
    break;

  case 12:
    move_link1(zone, 95);
    move_link2(zone, 85);
    break;

  case 13:
    move_base(zone, 60);
    move_link2(zone, 85);
    break;
  }
}

// Interrupt rutine produced by a packet received in the serial port
void serialEvent()
{
  while (Serial.available())
  {
    zone = Serial.readStringUntil('\n');
  }
}

void setup()
{
  // Pins for servo control
  claw.attach(50);
  base.attach(51);
  link1.attach(52);
  link2.attach(53);

  go_home(); // Initialize position for beginning the sequence
  move_base("2", 45);

  // Rate communication serial port
  Serial.begin(115200);
  delay(500);
}

void test_ang_speed()
{
  move_base(zone, 80);
  if (max_pos == true)
  {
    move_link2(zone, 80);
    max_pos = false;
  }
  else
  {
    move_link2(zone, 60);
    max_pos = true;
  }
  
}

void loop()
{
  test_ang_speed();
  delay(300);
  /*
  state++;
  Serial.println(zone);
  state_machine(zone, state);
  delay(500);

  if (state == 13)
  {
    state = 0;
  }*/
}