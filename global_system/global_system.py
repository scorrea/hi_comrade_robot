##########################################################################################
# THIS CODE WAS MADE BY: Sergio González Correa
# DATE: Lun, 6 Jun. 2020
# EMAIL: sergiogonzalezcorrea@gmail.com
##########################################################################################

import cv2
import time
import numpy as np
import serial
from random import randint
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from imutils import contours
from skimage import measure
import imutils
import math

# If you want to save the video recorded, save_video = True"
save_video = False 

# If you wanto to see graphs in real-time
graph_see = False 

# Pixel/distance(cm) ratio 
pix_dist = 1.3

# Obtain all the keypoints and append them to a list
def getKeypoints(probMap, threshold):

    mapSmooth = cv2.GaussianBlur(probMap, (3, 3), 0, 0)

    mapMask = np.uint8(mapSmooth > threshold)
    keypoints = []

    # Find the blobs
    contours, _ = cv2.findContours(mapMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # For each blob find the maxima
    for cnt in contours:
        blobMask = np.zeros(mapMask.shape)
        blobMask = cv2.fillConvexPoly(blobMask, cnt, 1)
        maskedProbMap = mapSmooth * blobMask
        _, _, _, maxLoc = cv2.minMaxLoc(maskedProbMap)
        keypoints.append(maxLoc + (probMap[maxLoc[1], maxLoc[0]],))

    return keypoints

# Find valid connections between the different joints of all persons present
def getValidPairs(output):

    valid_pairs = []
    invalid_pairs = []
    n_interp_samples = 10
    paf_score_th = 0.1
    conf_th = 0.7

    # Loop for every POSE_PAIR
    for k in range(len(mapIdx)): # Index of PAFS correspondientes a POSE_PAIRS
        # A -> B constitute a limb 
        pafA = output[0, mapIdx[k][0], :, :]
        pafB = output[0, mapIdx[k][1], :, :]
        pafA = cv2.resize(pafA, (frameWidth, frameHeight))
        pafB = cv2.resize(pafB, (frameWidth, frameHeight))

        # Find the keypoints for the first and second limb
        candA = detected_keypoints[POSE_PAIRS[k][0]]
        candB = detected_keypoints[POSE_PAIRS[k][1]]
        nA = len(candA)
        nB = len(candB)

        # If the keypoints for the joint-pair are detected
        # check every joint in candA with every joint in candB
        # calculate the distance vector between the two joints
        # find the PAF values at a set of interpolated points between the joints
        # use the above formula to compute a score to mark the connection valid

        if( nA != 0 and nB != 0):
            valid_pair = np.zeros((0, 3))

            for i in range(nA):
                max_j = -1
                maxScore = -1
                found = 0

                for j in range(nB):
                    # Find d_ij
                    d_ij = np.subtract(candB[j][:2], candA[i][:2])
                    norm = np.linalg.norm(d_ij)

                    if norm:
                        d_ij = d_ij / norm
                    else:
                        continue

                    # Find p(u)
                    interp_coord = list(zip(np.linspace(candA[i][0], candB[j][0], num=n_interp_samples),
                                            np.linspace(candA[i][1], candB[j][1], num=n_interp_samples)))
                    # Find L(p(u))
                    paf_interp = []

                    for k in range(len(interp_coord)):
                        paf_interp.append([pafA[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))],
                                           pafB[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))] ])
                    # Find E
                    paf_scores = np.dot(paf_interp, d_ij)
                    avg_paf_score = sum(paf_scores) / len(paf_scores)

                    # Check if the connection is valid
                    # If the fraction of interpolated vectors aligned with PAF is higher than threshold -> Valid Pair
                    if ( len(np.where(paf_scores > paf_score_th)[0]) / n_interp_samples ) > conf_th :
                        if avg_paf_score > maxScore:
                            max_j = j
                            maxScore = avg_paf_score
                            found = 1
                # Append the connection to the list
                if found:
                    valid_pair = np.append(valid_pair, [[candA[i][3], candB[max_j][3], maxScore]], axis=0)

            # Append the detected connections to the global list
            valid_pairs.append(valid_pair)
        else: # If no keypoints are detected
            invalid_pairs.append(k)
            valid_pairs.append([])

    return valid_pairs, invalid_pairs

# This function creates a list of keypoints belonging to each person
# For each detected valid pair, it assigns the joint(s) to a person
def getPersonwiseKeypoints(valid_pairs, invalid_pairs):
    # The last number in each row is the overall score
    personwiseKeypoints = -1 * np.ones((0, 19))

    for k in range(len(mapIdx)):
        if k not in invalid_pairs:
            partAs = valid_pairs[k][:, 0]
            partBs = valid_pairs[k][:, 1]
            indexA, indexB = np.array(POSE_PAIRS[k])

            for i in range(len(valid_pairs[k])):
                found = 0
                person_idx = -1

                for j in range(len(personwiseKeypoints)):
                    if personwiseKeypoints[j][indexA] == partAs[i]:
                        person_idx = j
                        found = 1
                        break

                if found:
                    personwiseKeypoints[person_idx][indexB] = partBs[i]
                    personwiseKeypoints[person_idx][-1] += keypoints_list[partBs[i].astype(int), 2] + valid_pairs[k][i][2]

                # If partA is not found in the subset, create a new subset
                elif not found and k < 17:
                    row = -1 * np.ones(19)
                    row[indexA] = partAs[i]
                    row[indexB] = partBs[i]
                    # Add the keypoint_scores for the two keypoints and the paf_score
                    row[-1] = sum(keypoints_list[valid_pairs[k][i, :2].astype(int), 2]) + valid_pairs[k][i][2]
                    personwiseKeypoints = np.vstack([personwiseKeypoints, row])

    return personwiseKeypoints

# Maximum possible distance with technician inside the cell 
max_module = (math.sqrt((640 / pix_dist) ** 2 + (480 / pix_dist) ** 2)) / pix_dist

# Function to calculate the zone of the perimeters the members are inside
def get_zone_pos(keypoints_loc, x_claw, y_claw):
    x = 0
    y = 0
    zone = ""
    module = []
    module_min = 0

    if not keypoints_loc:
        # If there are not any keypoints -> the technician is out of the cell
        # So, robotic arm can work at its maximum speed 
        zone = 3
        module_min = max_module
        
        return zone, module_min
 

    elif not x_claw:
        # If the claw is not detect, process stops for security reasons 
        zone = 0
        module_min = 0
        
        return zone, module_min

    else:
        for keypoints in keypoints_loc:

            x = x_claw - keypoints[0]
            y = y_claw - keypoints[1]

            # Check the radial distance between the claw and the technician 
            module.append(math.sqrt((x / pix_dist) ** 2 + (y / pix_dist) ** 2))

    module_min = min(module)
    
    # Check the location of the technician among the zones  
    if module_min < 185 / pix_dist: 
        zone = 0

    elif module_min < 275 / pix_dist: 
        zone = 1

    # At the moment the technician is inside the robotic cell
    elif module_min < max_module: 
        zone = 2

    else:
        module_min = max_module
        zone = 3

    return zone, module_min

# USB communication management with Arduino
class USB_comm():

    def __init__(self):
        try:
            # Connection pc-arduino by USB port
            self.arduino = serial.Serial('/dev/ttyACM0', 115200, timeout=1)

        except:
            print("ERROR. CHECK PORT CONNECTION")

    def send_order(self,mensaje):
        self.arduino.write(str.encode(mensaje + '\n'))

    def close (self):
        self.arduino.close()

# Function to send zone to Arduino
def send_order_arm(zone): 
    mensaje = USB_comm()
    mensaje.send_order(str(zone))
    mensaje.close()

# Send the zone where the nearest joint to the claw is ubicated 
def get_links_pos(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    # Threshold the image to reveal light regions in the
    # blurred image
    thresh = cv2.threshold(blurred, 248, 255, cv2.THRESH_BINARY)[1]

    # Perform erosions and dilations to remove blobs of noise
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=4)

    # Perform a connected component analysis on the thresholded image
    labels = measure.label(thresh, connectivity=2, background=0)
    # Store them in a mask
    mask = np.zeros(thresh.shape, dtype="uint8")

    # Loop over the unique components
    for label in np.unique(labels):
        # If this is the background label, ignore it
        if label == 0:
            continue

        # Otherwise, construct the label mask and count the
        # number of pixels
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)

        # If the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"

        if numPixels > 100 and numPixels < 700: #update the size for the room
            mask = cv2.add(mask, labelMask)

    # Find the contours in the mask, then sort them from left to right
    detected = True

    try:
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        cnts = contours.sort_contours(cnts)[0] # sort contours from left to right 

    except:
        detected = False
        print ("LEDs not detected")

    leds_arms = []

    if detected == True:
        try:
            for i in range(0,3):
                leds_arms.append(cnts[i])
                # Save only the first 3  in the left-bottom zone 
                # The first one is the led in the base and the last one 
                # is the led in the claw
    
        except:
            print("Some LED is missed")

    x_list = []
    y_list = []

    for (i, c) in enumerate(leds_arms):

        # Draw the bright spot on the image
        (x, y, _, _) = cv2.boundingRect(c)
        # Find the centre of the brigth point, which is the ubication of the link
        ((cX, cY), radius) = cv2.minEnclosingCircle(c)
        cv2.circle(frame, (int(cX), int(cY)), int(radius), (0, 0, 255), 3)
        cv2.putText(frame, "#{}".format(i + 1), (x, y - 15), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
        x_list.append(int(cX))
        y_list.append(int(cY))

    return x_list, y_list

x_dist = []
y_vel_l1 = []
y_vel_l2 = []
x_time = []
firstone = True

# Function for save data if it's correctly acquired
def save_data(ang_vel, distance, time):
    
    x_dist.append(distance)
    y_vel_l1.append(ang_vel[0])
    y_vel_l2.append(ang_vel[1])
    x_time.append(time)

last_angle = [0, 0]

# Function to calculate angular velocity of the servos
def get_links_vel(x_link, y_link, time, last_time):

    link_1 = [x_link[1] - x_link[0], y_link[1] - y_link[0]]
    link_2 = [x_link[2] - x_link[1], y_link[2] - y_link[1]]

    angle = [0, 0]
    ang_vel = [0, 0]

    angle[0] = math.atan(link_1[1] / link_1[0]) # angle link1
    angle[1] = math.atan(link_2[1] / link_2[0]) # angle link2

    ang_vel[0] = abs((angle[0] - last_angle[0]) / (time - last_time))
    ang_vel[1] = abs((angle[1] - last_angle[1]) / (time - last_time))

    last_angle[0] = angle[0]
    last_angle[1] = angle[1]

    last_time = time

    return ang_vel, last_time

xtime_z = []
y_z = []
y_ang_speed = []

# Function to plot ubication zone per second
def plot_zone(zone, time):

    plt.style.use('fivethirtyeight')
    fig = plt.figure(1)
    plt.title('Claw distance by zones')
    zone_patch = mpatches.Patch(color='purple', label='Zone')
    plt.legend(handles=[zone_patch])
    plt.ylim(0, 3)
    plt.xlim(time - 20, time + 5)
    plt.xlabel("Zone")
    plt.ylabel("Time (s)")

    xtime_z.append(time) # Time data
    y_z.append(zone) # Zone data
    plt.plot(xtime_z, y_z, 'purple')
    fig.canvas.draw()

    img = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)

    return img

# Function to sort data by distance from the nearest to the furthest
def sort_saved_data(y_vel, x_distance):
    # Sort the distance data and obtain the index sorted too
    index_sorted = np.argsort(x_distance)

    # List to save the data sorted 
    y_sorted = []
    x_sorted = []

    for i in index_sorted: # Save the data sorted inside the previous lists
        y_sorted.append(y_vel[i])
        x_sorted.append(x_distance[i])

    return y_sorted, x_sorted

# Function to plot angular speed of link1, the longest link
def plot_ang_l1(y_vel_l1, x_dist):

    plt.style.use('fivethirtyeight')
    fig = plt.figure(2, figsize = (20, 10))
    plt.title('ω of link1 - Distance')
    zone_patch = mpatches.Patch(color= 'red', label= 'ω servo 1', linewidth= 1)
    #plt.xlim(x_dist[0], x_dist[-1])
    #plt.ylim(0, 0.6)
    plt.legend(handles = [zone_patch])
    plt.ylabel("ω1 (rad/s)")
    plt.xlabel("Distance (cm)")
    
    plt.scatter(x_dist, y_vel_l1, c = 'red', alpha= 1)
    m, b = np.polyfit(x_dist, y_vel_l1, 1)
    plt.plot(np.array(x_dist), m * np.array(x_dist) + b, c= "purple")
    fig.canvas.draw()
    plt.show()

# Function to plot angular speed of link2, the link from link 1 to the claw
def plot_ang_l2(y_vel_l2, x_dist):

    plt.style.use('fivethirtyeight')
    plt.figure(3, figsize = (20, 10))
    plt.title('ω of link2 - Distance')
    zone_patch = mpatches.Patch(color= 'blue', label= 'ω servo 2', linewidth= 1)
    #plt.xlim(x_dist[0], x_dist[-1])
    #plt.ylim(0, 0.6)
    plt.legend(handles = [zone_patch])
    plt.ylabel("ω2 (rad/s)")
    plt.xlabel("Distance (cm)")
    
    plt.scatter(x_dist, y_vel_l2, c= 'blue', alpha = 0.5)
    m, b = np.polyfit(x_dist, y_vel_l2, 1)
    plt.plot(np.array(x_dist), m * np.array(x_dist) + b, c= "purple")
    plt.show()

# Function to plot angular speed of links in function of time 
def plot_ang_time(y_speed, time):

    plt.style.use('fivethirtyeight')
    fig = plt.figure(4, figsize = (20, 10))
    plt.title('ω of link 2 - Time')
    zone_patch = mpatches.Patch(color = 'orange', label = 'ω servo 2', linewidth = 1)
    plt.legend(handles = [zone_patch])
    #plt.xlim(3, time[-1])
    #plt.ylim(0, 0.6)
    plt.ylabel("ω (rad/s)")
    plt.xlabel("Time (s)")
    
    plt.plot(time, y_speed, 'orange')
    fig.canvas.draw()
    plt.show()

if __name__ == "__main__":

    # Path to the directory where the video will be saved
    path_video = "/home/sergiocorrea/tfg_code/system_2.avi"

    protoFile = "/home/sergiocorrea/tfg_code/system_code/openpose/pose/coco/pose_deploy_linevec.prototxt"
    weightsFile = "/home/sergiocorrea/tfg_code/system_code/openpose/pose/coco/pose_iter_440000.caffemodel"
    nPoints = 18
    keypointsMapping = ['Nose', 'Neck', 'R-Sho', 'R-Elb', 'R-Wr', 'L-Sho', 'L-Elb', 'L-Wr', 'R-Hip', 'R-Knee', 'R-Ank', 'L-Hip', 'L-Knee', 'L-Ank', 'R-Eye', 'L-Eye', 'R-Ear', 'L-Ear']

    POSE_PAIRS = [[1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7],
                  [1, 8], [8, 9], [9, 10], [1, 11], [11, 12], [12, 13],
                  [1, 0], [0, 14], [14, 16], [0, 15], [15, 17],
                  [2, 17], [5, 16]] 

    # Possible joints of the detected body joints 
    # For example: neck and right shoulder => [1, 2]

    # Index of PAFs correspoding to the POSE_PAIRS
    # e.g for POSE_PAIR(1,2), the PAFs are located at indices (31,32) of output, Similarly, (1,5) -> (39,40) and so on.
    mapIdx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44],
              [19, 20], [21, 22], [23, 24], [25, 26], [27, 28], [29, 30],
              [47, 48], [49, 50], [53, 54], [51, 52], [55, 56],
              [37, 38], [45, 46]]

    colors = [[0, 192, 255], [0, 255, 255], [0, 0, 255], [0, 0, 192], [80, 208, 146], [80, 176, 0],
              [238, 215, 189], [255, 200, 100], [255, 0, 255], [0, 255, 0], [255, 200, 100], [255, 0, 255],
              [0, 0, 255], [255, 0, 0], [200, 200, 0], [255, 0, 0], [200, 200, 0], [0, 0, 0]]

    # Load the openpose model for doing inference later 
    net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)
    # Configuration OpenCV for doing the inference in the GPU 
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

    inHeight = 368
    threshold = 0.45 # Level of confidence necessary to be considered as a keypoint 
    font = cv2.FONT_HERSHEY_PLAIN

    # Selection of webcam for capturing video and 
    # Selection the pixel type of the frames
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))

    if save_video == True:
        if graph_see == True: 
            # To save the video of the frames captured by webcam next
            # to the graph of zone-time
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            out = cv2.VideoWriter(path_video, fourcc, 20.0, (1280, 480))
        else:
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            out = cv2.VideoWriter(path_video, fourcc, 20.0, (640, 480))

    # Adjust resolution of the imagen captured by the webcam 
    cap.set(3, 640)
    cap.set(4, 480)

    # Setting frame-rate of the webcam
    cap.set(cv2.CAP_PROP_FPS, 10)

    # Check if the webcam is avaliable
    if not cap.isOpened():
        raise IOError("Cannot open webcam")
    
    last_time = time.process_time()
    while True:
        ret, image1 = cap.read()
        last = time.time()

        # Obtain the positions of the links with the light detector
        x_link, y_link = get_links_pos(image1)

        frameWidth = image1.shape[1]
        frameHeight = image1.shape[0]

        # inHeight * aspect ratio of the frame
        inWidth = int((inHeight / frameHeight) * frameWidth) 
        inpBlob = cv2.dnn.blobFromImage(image1, 1.0 / 255, (inWidth, inHeight),
                              (0, 0, 0), swapRB = False, crop = False)

        net.setInput(inpBlob)
        output = net.forward()

        detected_keypoints = [] # List to store keypoints detected by openpose 
        keypoints_list = np.zeros((0, 3))
        keypoint_id = 0

        keypoints_detected = []

        for part in range(nPoints):

            probMap = output[0, part, :, :]
            probMap = cv2.resize(probMap, (image1.shape[1], image1.shape[0]))
            keypoints = getKeypoints(probMap, threshold)

            keypoints_with_id = []

            for i in range(len(keypoints)):
                keypoints_with_id.append(keypoints[i] + (keypoint_id,))
                keypoints_list = np.vstack([keypoints_list, keypoints[i]])
                keypoint_id += 1

            detected_keypoints.append(keypoints_with_id)

        frameClone = image1.copy()

        keypoints_zone = []

        for i in range(nPoints):
            for j in range(len(detected_keypoints[i])):            
                # Selection of the necessary keypoints --> (1, 2, 3, 4, 5, 6, 7)
                # Show their position with circles over the frame
                
                if i > 0 and i < 8:
                    keypoints_zone.append(detected_keypoints[i][j][0:2])
                    cv2.circle(frameClone, detected_keypoints[i][j][0:2], 5, colors[i], -1, cv2.LINE_AA)

        try:
            # Obtain the zone corresponding to the technician's current position
            zone, distance = get_zone_pos(keypoints_zone, x_link[2], y_link[2]) 
            print("\nZONE " + str(zone))
            print("\nDISTANCE " + str(distance))
            current_time = time.process_time()

            # Calculate current angular speed of the links of the robotic arm
            ang_vel, last_time = get_links_vel(x_link, y_link, current_time, last_time)

        except:
            zone = 0
            print("ERROR")

        if graph_see == True:
            # Show a graph of zone-time during the execution of the program
            img = plot_zone(zone, current_time)        

        
        try:
            # Save the data acquired
            save_data(ang_vel, distance, current_time)
        except:
            print("ERROR: Couldn't save data")
            
        send_order_arm(zone)        

        valid_pairs, invalid_pairs = getValidPairs(output) 
        # Find valid connections between the different joints of all persons present

        # List of keypoints of the same body
        personwiseKeypoints = getPersonwiseKeypoints(valid_pairs, invalid_pairs)

        for i in range(nPoints - 1):
            for n in range(len(personwiseKeypoints)):

                if i <= 5:
                    index = personwiseKeypoints[n][np.array(POSE_PAIRS[i])]

                    if -1 in index:
                        continue

                    B = np.int32(keypoints_list[index.astype(int), 0]) 
                    # B is the x coordinates of the start and end points of each body part

                    A = np.int32(keypoints_list[index.astype(int), 1]) 
                    # A is the x coordinates of the start and end points of each body part

                    cv2.line(frameClone, (B[0], A[0]), (B[1], A[1]), colors[i], 3, cv2.LINE_AA) #vector

        # Calculate and show in the frame the frame per second ratio
        elapse_time = time.time() - last
        fps = 1 / elapse_time
        cv2.putText(frameClone, "FPS: " + str(round(fps, 2)), (10, 30), font, 2, (0, 0, 255), 1)

        # To see the zone-time graph in real time 
        if graph_see == True:
            frameClone = np.concatenate((frameClone, img), axis = 1)
        
        cv2.imshow('Monitorization', frameClone)

        # To save the video of the system working 
        if save_video == True:
            out.write(frameClone)

        c = cv2.waitKey(1)
        if c == 27:
            # When the execution of the program has ended, stop the arm
            send_order_arm("0") 
            break

    # Plot graph of the angular velocity during the time
    plot_ang_time(y_vel_l2, x_time)

    # Sort the data by the minimum to the maximum distance between technician and arm 
    y_vel_l1, _ = sort_saved_data(y_vel_l1, x_dist)
    y_vel_l2, x_dist = sort_saved_data(y_vel_l2, x_dist)

    # Plot graphs of angular velocity of servos in function of the distance from the keypoint to the claw
    plot_ang_l1(y_vel_l1, x_dist)
    plot_ang_l2(y_vel_l2, x_dist)

    cap.release()
    cv2.destroyAllWindows()
