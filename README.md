# Aplicación de Técnicas de Inteligencia Artificial para el Seguimiento de Personas en Celdas Robotizadas de Espacio Compartido

_El presente proyecto fue desarrollado con el objetivo de realizar una comunicación hombre-máquina más natural y sencilla. Valiéndose de técnicas de inteligencia artificial, el presente sistema realiza un seguimiento de la persona mediante la optención de su pose fotograma a fotograma, además, es capaz de realizar un reconocimiento gestual entre gestos de marcha, parada, subir velocidad y bajar velocidad._

## 🚀 Comencemos 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### 📋 Pre-requisitos 📋

_Requisitos empleados para su desarrollo_

- **OS:   UBUNTU 18.04 LTS.** Para el desarrollo y pruebas del sistema, se empleó en todo momento UBUNTU 18.04 LTS, sin embargo el software usado es multiplataforma. Posiblemente haya que adaptar versiones.

- **GPU:  NVIDIA GTX 1070.** Medio de inferencia y de entrenamiento usado para el presente proyecto. Se consigue un máximo de 5,5 FPS ejecutando el sistema en tiempo real. Se recomienda el uso de una GPU de mayor rango para el uso del sistema en tiempo real. 

- **CPU:  Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz.** CPU de la máquina empleada.

- **RAM.** Se recomiendan 16GB, fue la empleada durante el desarrollo. 

- **Anaconda: versión 4.8.3 o superior**. Para replicar el entorno virtual usando el archivo tfg_env.yml.

- **Cámara Web.** Por lo menos debe grabar a 480p con formato de píxeles MJPEG.

- **SSD.** Medio de almacenamiento para la instalación del software. 

### 🔧 Instalación 🔧


Para una **máquina** que emplea una GPU NVIDIA **GTX 1070**

- _Instalación drivers GPU._ Usado: **440.100**

- _Instalación CUDA 10.1 y cudDNN 7.6.5_

- _Instalación OpenCV 4.2.0_

- _Instalación Anaconda 4.8.3_

- _Uso tfg_env.yml para la creación de un entorno virtual con todas las librerías. Tutorial: [Conda Envs Management](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)_
```
Es necesario modificar el path dentro del tfg_env.yml por el del entorno virtual en el que se desea instalar en la máquina a emplear
 ```

### 📦 Carpetas del repositorio 📦

- **class_gesture.** Carpeta con el código para realizar la inferencia, por lotes de imágenes, del sistema de reconocimiento gestual.

- **global_system.** Unión del código de los distintos algoritmos para el funcionamiento del sistema global.

- **robot_control.** Proyecto Arduino para la programación de un brazo robótico impreso en 3D, controlado por servomotores.

- **sub_algorithms.** Conjunto de algoritmos desarrollados durante el proyecto. Separados en distintos scripts.


## ⚙️ Mejoras del sistema ⚙️

_Para un aumento de la eficiencia y el frame rate, es necesario realizar una adaptación del código a C++ y realizar una optimización en la inferencia con [TensorRT](https://developer.nvidia.com/tensorrt)_


## 🛠️ Software de desarrollo 🛠️

_Herramientas empleadas:_

* [Anaconda](https://www.anaconda.com/products/individual) - Gestor de entornos virtuales
* [Keras](https://keras.io/) - Framework desarrollo red neuronal
* [Atom](https://atom.io/) 
* [OpenCV](https://opencv.org/) - Biblioteca para visión artificial

## ✒️ Autor ✒️

✒ **Sergio González Correa** - *Desarrollador principal* - [scorrea](https://gitlab.com/scorrea)

## 🎁 Agradecimientos 🎁

* Muchísimas gracias a mis tutores, por todo lo que he aprendido gracias a vosotros durante el transcurso del proyecto. Gracias por todo. 
