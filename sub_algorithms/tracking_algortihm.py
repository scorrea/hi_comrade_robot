##########################################################################################
# THIS CODE WAS MADE BY: Sergio González Correa
# DATE: Lun, 6 Jun. 2020
# EMAIL: sergiogonzalezcorrea@gmail.com
##########################################################################################

import cv2
import time
import numpy as np

# Obtain all the keypoints and append them to a list
def getKeypoints(probMap, threshold):

    mapSmooth = cv2.GaussianBlur(probMap,(3,3),0,0)

    mapMask = np.uint8(mapSmooth>threshold)
    keypoints = []

    # find the blobs
    contours, _ = cv2.findContours(mapMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # for each blob find the maxima
    for cnt in contours:
        blobMask = np.zeros(mapMask.shape)
        blobMask = cv2.fillConvexPoly(blobMask, cnt, 1)
        maskedProbMap = mapSmooth * blobMask
        _, _, _, maxLoc = cv2.minMaxLoc(maskedProbMap)
        keypoints.append(maxLoc + (probMap[maxLoc[1], maxLoc[0]],))

    return keypoints

# Find valid connections between the different joints of a all persons present
def getValidPairs(output):

    valid_pairs = []
    invalid_pairs = []
    n_interp_samples = 10
    paf_score_th = 0.1
    conf_th = 0.7

    # loop for every POSE_PAIR
    for k in range(len(mapIdx)): # index of PAFS correspondientes a POSE_PAIRS
        # A->B constitute a limb
        pafA = output[0, mapIdx[k][0], :, :]
        pafB = output[0, mapIdx[k][1], :, :]
        pafA = cv2.resize(pafA, (frameWidth, frameHeight))
        pafB = cv2.resize(pafB, (frameWidth, frameHeight))

        # find the keypoints for the first and second limb
        candA = detected_keypoints[POSE_PAIRS[k][0]]
        candB = detected_keypoints[POSE_PAIRS[k][1]]
        nA = len(candA)
        nB = len(candB)

        # if keypoints for the joint-pair is detected
        # check every joint in candA with every joint in candB
        # calculate the distance vector between the two joints
        # find the PAF values at a set of interpolated points between the joints
        # use the above formula to compute a score to mark the connection valid

        if( nA != 0 and nB != 0):
            valid_pair = np.zeros((0,3))

            for i in range(nA):
                max_j=-1
                maxScore = -1
                found = 0

                for j in range(nB):
                    # find d_ij
                    d_ij = np.subtract(candB[j][:2], candA[i][:2])
                    norm = np.linalg.norm(d_ij)

                    if norm:
                        d_ij = d_ij / norm
                    else:
                        continue

                    # find p(u)
                    interp_coord = list(zip(np.linspace(candA[i][0], candB[j][0], num=n_interp_samples),
                                            np.linspace(candA[i][1], candB[j][1], num=n_interp_samples)))
                    # find L(p(u))
                    paf_interp = []

                    for k in range(len(interp_coord)):
                        paf_interp.append([pafA[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))],
                                           pafB[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))] ])
                    # find E
                    paf_scores = np.dot(paf_interp, d_ij)
                    avg_paf_score = sum(paf_scores)/len(paf_scores)

                    # check if the connection is valid
                    # if the fraction of interpolated vectors aligned with PAF is higher then threshold -> Valid Pair
                    if ( len(np.where(paf_scores > paf_score_th)[0]) / n_interp_samples ) > conf_th :
                        if avg_paf_score > maxScore:
                            max_j = j
                            maxScore = avg_paf_score
                            found = 1
                # append the connection to the list
                if found:
                    valid_pair = np.append(valid_pair, [[candA[i][3], candB[max_j][3], maxScore]], axis=0)

            # append the detected connections to the global list
            valid_pairs.append(valid_pair)
        else: # if no keypoints are detected
            invalid_pairs.append(k)
            valid_pairs.append([])

    return valid_pairs, invalid_pairs

# This function creates a list of keypoints belonging to each person
# for each detected valid pair, it assigns the joint(s) to a person
def getPersonwiseKeypoints(valid_pairs, invalid_pairs):
    # the last number in each row is the overall score
    personwiseKeypoints = -1 * np.ones((0, 19))

    for k in range(len(mapIdx)):
        if k not in invalid_pairs:
            partAs = valid_pairs[k][:,0]
            partBs = valid_pairs[k][:,1]
            indexA, indexB = np.array(POSE_PAIRS[k])

            for i in range(len(valid_pairs[k])):
                found = 0
                person_idx = -1

                for j in range(len(personwiseKeypoints)):
                    if personwiseKeypoints[j][indexA] == partAs[i]:
                        person_idx = j
                        found = 1
                        break

                if found:
                    personwiseKeypoints[person_idx][indexB] = partBs[i]
                    personwiseKeypoints[person_idx][-1] += keypoints_list[partBs[i].astype(int), 2] + valid_pairs[k][i][2]

                # if find no partA in the subset, create a new subset
                elif not found and k < 17:
                    row = -1 * np.ones(19)
                    row[indexA] = partAs[i]
                    row[indexB] = partBs[i]
                    # add the keypoint_scores for the two keypoints and the paf_score
                    row[-1] = sum(keypoints_list[valid_pairs[k][i,:2].astype(int), 2]) + valid_pairs[k][i][2]
                    personwiseKeypoints = np.vstack([personwiseKeypoints, row])

    return personwiseKeypoints

if __name__ == "__main__":

    path_video = "/home/sergiocorrea/tfg_code/figures_results/output.avi"

    path_op_fig = "/home/sergiocorrea/tfg_code/figures_results/openpose"

    protoFile = "/home/sergiocorrea/tfg_code/system_code/openpose/pose/coco/pose_deploy_linevec.prototxt"
    weightsFile = "/home/sergiocorrea/tfg_code/system_code/openpose/pose/coco/pose_iter_440000.caffemodel"
    nPoints = 18
    keypointsMapping = ['Nose', 'Neck', 'R-Sho', 'R-Elb', 'R-Wr', 'L-Sho', 'L-Elb', 'L-Wr', 'R-Hip', 'R-Knee', 'R-Ank', 'L-Hip', 'L-Knee', 'L-Ank', 'R-Eye', 'L-Eye', 'R-Ear', 'L-Ear']

    POSE_PAIRS = [[1, 2], [1, 5], [2, 3], [3, 4], [5, 6], [6, 7],
                    [1, 8], [8, 9], [9, 10], [1, 11], [11, 12], [12, 13],
                [1, 0], [0, 14], [14, 16], [0, 15], [15, 17],
                [2, 17], [5, 16] ] 
    # possible joints of the detected body joints 
    # for example: neck and right shoulder => [1, 2]

    # index of pafs correspoding to the POSE_PAIRS
    # e.g for POSE_PAIR(1,2), the PAFs are located at indices (31,32) of output, Similarly, (1,5) -> (39,40) and so on.
    mapIdx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44],
                [19, 20], [21, 22], [23, 24], [25, 26], [27, 28], [29, 30],
                [47, 48], [49, 50], [53, 54], [51, 52], [55, 56],
                [37, 38], [45, 46]]

    colors = [[0,192,255],  [0,255,255], [0,0,255], [0, 0, 192], [80,208,146], [80,176,0],
                [0, 255, 0], [255, 200, 100], [255, 0, 255], [0, 255, 0], [255, 200, 100], [255, 0, 255],
                [0, 0, 255], [255, 0, 0], [200, 200, 0], [255, 0, 0], [200, 200, 0], [0, 0, 0]]

    # load the openpose model for doing inference later 
    net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)
    # configuration OpenCV for doing the inference in the GPU 
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)

    inHeight = 368
    threshold = 0.25 # level of confidence necessary to be considered as a keypoint 
    font = cv2.FONT_HERSHEY_PLAIN

    # selection of webcam for capturing video and 
    # selection the pixel type of the frames
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(path_video, fourcc, 20.0, (640, 480))

    # adjust resolution of the imagen captured by the webcam 
    cap.set(3, 640)
    cap.set(4, 480)

    # setting frame-rate of the webcam
    cap.set(cv2.CAP_PROP_FPS, 10)

    # check if the webcam is avaliable
    if not cap.isOpened():
        raise IOError("Cannot open webcam")

    fig = 0

    while True:
        starting_time = time.time()
        ret, image1 = cap.read()

        frameWidth = image1.shape[1]
        frameHeight = image1.shape[0]

        # inHeight * aspect ratio of the frame
        inWidth = int((inHeight / frameHeight) * frameWidth) 
        inpBlob = cv2.dnn.blobFromImage(image1, 1.0 / 255, (inWidth, inHeight),
                                (0, 0, 0), swapRB = False, crop = False)

        net.setInput(inpBlob)
        output = net.forward()

        detected_keypoints = [] # list to store keypoints detected by openpose 
        keypoints_list = np.zeros((0, 3))
        keypoint_id = 0

        keypoints_detected = []

        for part in range(nPoints):

            probMap = output[0, part,:,:]
            probMap = cv2.resize(probMap, (image1.shape[1], image1.shape[0]))
            keypoints = getKeypoints(probMap, threshold)

            keypoints_with_id = []

            for i in range(len(keypoints)):
                keypoints_with_id.append(keypoints[i] + (keypoint_id,))
                keypoints_list = np.vstack([keypoints_list, keypoints[i]])
                keypoint_id += 1

            detected_keypoints.append(keypoints_with_id)

        frameClone = image1.copy()

        keypoints_zone = []

        for i in range(nPoints):
            for j in range(len(detected_keypoints[i])):            
                # selection of the necessary keypoints --> (1, 2, 3, 4, 5, 6, 7)
                # show their position with circles over the frame
                
                if i > 0 and i < 8:
                    keypoints_zone.append(detected_keypoints[i][j][0:2])
                    cv2.circle(frameClone, detected_keypoints[i][j][0:2], 5, colors[i], -1, cv2.LINE_AA)      

        valid_pairs, invalid_pairs = getValidPairs(output) 
        # find valid connections between the different joints of a all persons present

        # list of keypoints of the same body
        personwiseKeypoints = getPersonwiseKeypoints(valid_pairs, invalid_pairs)

        for i in range(nPoints - 1):
            for n in range(len(personwiseKeypoints)):

                if i <= 5:
                    index = personwiseKeypoints[n][np.array(POSE_PAIRS[i])]

                    if -1 in index:
                        continue

                    B = np.int32(keypoints_list[index.astype(int), 0]) 
                    # B is the x coordinates of the start and end points of each body part

                    A = np.int32(keypoints_list[index.astype(int), 1]) 
                    # A is the x coordinates of the start and end points of each body part

                    cv2.line(frameClone, (B[0], A[0]), (B[1], A[1]), colors[i], 3, cv2.LINE_AA) #vector

        # calculate and show in the frame the frame per second ratio
        elapse_time = time.time() - starting_time
        fps = 1 / elapse_time
        cv2.putText(frameClone, "FPS: " + str(round(fps, 2)),(10, 30), font, 2, (0, 0, 255), 1)
        
        cv2.imshow('Monitorization', frameClone)
        out.write(frameClone)

        c = cv2.waitKey(1)
        if c == 32:
            fig += 1
            cv2.imwrite(path_op_fig + str(fig) + ".jpg", frameClone)
        
        elif c == 27: 
            break

    cap.release()
    cv2.destroyAllWindows()
