##########################################################################################
# THIS CODE WAS MADE BY: Sergio González Correa
# DATE: Lun, 6 Jun. 2020
# EMAIL: sergiogonzalezcorrea@gmail.com
##########################################################################################

import cv2
import time
import numpy as np
from random import randint
from imutils import contours
from skimage import measure
import imutils
import math


def get_links_pos(frame):

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    # threshold the image to reveal light regions in the
    # blurred image

    # Si tiene problemas de detectar puntos luminosos por culpa de reflejos de cristales
    thresh = cv2.threshold(blurred, 240, 255, cv2.THRESH_BINARY)[1]

    #cv2.imshow("with noise", thresh)

    # perform a series of erosions and dilations to remove
    # any small blobs of noise from the thresholded image
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=4)

    # perform a connected component analysis on the thresholded
    # image, then initialize a mask to store only the "large"
    # components
    labels = measure.label(thresh, connectivity=2, background=0)
    mask = np.zeros(thresh.shape, dtype="uint8")

    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue

            # otherwise, construct the label mask and count the
            # number of pixels
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)

        # if the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"

        if numPixels > 100 and numPixels < 1000:  # update for the room
            mask = cv2.add(mask, labelMask)
            cv2.imshow('mask', mask)

        # find the contours in the mask, then sort them from left to
        # right

        try:
            cnts = cv2.findContours(
                mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts)
            cnts = contours.sort_contours(cnts)[0]

        except:
            print("Claw out of frame plane")

        # loop over the contours
        for (i, c) in enumerate(cnts):
            # draw the bright spot on the image
            (x, y, w, h) = cv2.boundingRect(c)
            print(str(x) + " " + str(y) + " " + str(w) + " " + str(h))
            ((cX, cY), radius) = cv2.minEnclosingCircle(c)
            cv2.circle(frame, (int(cX), int(cY)), int(radius), (0, 0, 255), 3)
            cv2.putText(frame, "#{}".format(i + 1), (x, y - 15),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)

    return frame, mask


if __name__ == "__main__":

    path_light_fig = "/home/sergiocorrea/tfg_code/figures_results/light_detector"

    font = cv2.FONT_HERSHEY_PLAIN

    # selection of webcam for capturing video and
    # selection the pixel type of the frames
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))

    # adjust resolution of the imagen captured by the webcam
    cap.set(3, 640)
    cap.set(4, 480)

    # setting frame-rate of the webcam
    cap.set(cv2.CAP_PROP_FPS, 10)

    # check if the webcam is avaliable
    if not cap.isOpened():
        raise IOError("Cannot open webcam")

    fig = 20

    while True:
        starting_time = time.time()
        ret, image1 = cap.read()

        frame = image1.copy()

        try:
            frameClone, mask = get_links_pos(image1)

        except:
            print("no leds")

        # calculate and show in the frame the frame per second ratio
        elapse_time = time.time() - starting_time
        fps = 1 / elapse_time
        #cv2.putText(frameClone, "FPS: " + str(round(fps, 2)),(10, 30), font, 2, (0, 0, 255), 1)

        try:
            cv2.imshow('Monitorization', frameClone)
        except:
            print("no leds")

        c = cv2.waitKey(1)
        if c == 32:
            fig += 1
            cv2.imwrite(path_light_fig + str(fig) + ".jpg", frame)
            cv2.imwrite(path_light_fig + str(fig) + "_leds_d.jpg", frameClone)
            cv2.imwrite(path_light_fig + str(fig) + "_mask.jpg", mask)

        elif c == 27:
            break

    cap.release()
    cv2.destroyAllWindows()
