##########################################################################################
# THIS CODE WAS MADE BY: Sergio González Correa
# DATE: Lun, 6 Jun. 2020
# EMAIL: sergiogonzalezcorrea@gmail.com
##########################################################################################


import cv2
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model
from keras.models import Model
from keras.preprocessing.image import load_img
import glob
import tensorflow as tf
import pandas as pd 
from tabulate import tabulate


# Ensure the hardware of inference is the GPU
gpus = tf.config.experimental.list_physical_devices('GPU')

if gpus:
    try:
        # Restrict TensorFlow to only use the GPU
        tf.config.experimental.set_visible_devices(gpus[0], 'GPU')

        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")

    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)


# Load h5 file with the weights and architecture of the cnn from the training process
model_weights = 'classificator.h5'

gestures_list = ['Marcha', 'Parada',
                 'Mas velocidad', 'Menos velocidad', 'Sin gesto']

class_gestures = [0, 1, 2, 3, 4]

path_files = "/home/sergiocorrea/data/frames_for_inference/{}"
path_save_images = "/home/sergiocorrea/tfg_code/figures_results/cnn_class/"
path_results_gestures = "/home/sergiocorrea/tfg_code/results/model_inference.csv"
path_results_stop = "/home/sergiocorrea/tfg_code/results/model_stop_results.csv"

# Use a pandas dataframe to save results and data generated during the inference process 
# Creating an empty Dataframe with column names only for append the results in rows as they are generated
data = pd.DataFrame(columns=[' ', 'Imagenes Clase', gestures_list[0], gestures_list[1], gestures_list[2],
                             gestures_list[3], gestures_list[4], 'Precision (%)'])

stop_results = pd.DataFrame(columns=[' ', 'Imagenes Clase', gestures_list[0], gestures_list[1], gestures_list[2],
                             gestures_list[3], gestures_list[4], 'Precision (%)'])
# Load paths of images for test the cnn in numerical order, to compare the original pictures with 
# the pictures classificated
go = sorted(glob.glob(path_files.format("go_*")))
stop = sorted(glob.glob(path_files.format("stop_*")))
more_speed = sorted(glob.glob(path_files.format("more_speed_*")))
less_speed = sorted(glob.glob(path_files.format("less_speed_*")))
no_gesture = sorted(glob.glob(path_files.format("no*")))
stop_left = sorted(glob.glob(path_files.format("left/stop_*")))
stop_right = sorted(glob.glob(path_files.format("right/stop_*")))
stop_both = sorted(glob.glob(path_files.format("both/stop_*")))


# Load cnn for gesture classification with pretrained weights
def load_pretrained_model():

    model = load_model(model_weights, compile=True)
    model.summary()

    return model

# Preprocess and load the images for inference
def _preprocess_(batch):
    processed = []

    batch_x = [cv2.imread(img) for img in batch]

    batch_x = [cv2.resize(img, (244, 244), interpolation=cv2.INTER_AREA)
               for img in batch_x]

    batch_x = [img.astype(np.float32) / 255. for img in batch_x]

    batch_x = [np.array(img) for img in batch_x]

    return batch_x


# Load batch of images for inference in an array
def gesture_load(gesture):

    if gesture == "stop":
        return np.array(_preprocess_(stop))

    if gesture == "go":
        return np.array(_preprocess_(go))

    if gesture == "more_speed":
        return np.array(_preprocess_(more_speed))

    if gesture == "less_speed":
        return np.array(_preprocess_(less_speed))

    if gesture == "no_gesture":
        return np.array(_preprocess_(no_gesture))

    else:
        if gesture == "right":
            return np.array(_preprocess_(stop_right))
        
        if gesture == "left":
            return np.array(_preprocess_(stop_left))
        
        if gesture == "both":
            return np.array(_preprocess_(stop_both))


gesture = [None, None, None, None, None, None, None, None]

# Function to detect if this is a gesture by a confidence interval
def is_gesture(gesture_det):

    gesture.pop(0)  # Delete the first position of the list
    gesture.append(gesture_det)
    actual_gest = gesture[-1]
    is_gesture = True

    for gest in gesture:  # Test if this frame and the last 7 have the same gesture
        if gest != actual_gest:
            is_gesture = False

    return is_gesture


# Function to detect class from model with model probability
def gesture_predicted(prediction):

    [np.argmax(pred) for pred in prediction]
    # Class with more probability is the gesture of that frame
    return [np.argmax(pred) for pred in prediction]


# Arm control according to the gesture
def gesture_control(gesture, is_gesture, zone):

    if is_gesture != True:  # Check if it's a gesture
        return

    elif gesture == gestures_list[0]:
        # Go, initialized to zone 1 if it's possible due to safety
        zone = 1

    elif gesture == gestures_list[1]:
        # Stop, same situation as the operator is next to the arm
        zone = 0

    elif gesture == gestures_list[2]:
        # More speed, if it is possible increase the speed due to
        # the zone of security
        gesture = [None, None, None, None, None, None, None, None]

        if zone <= 3:
            zone += 1

    elif gesture == gestures_list[3]:
        # Less speed, reduce speed if it is not null
        gesture = [None, None, None, None, None, None, None, None]
        if zone != 0:
            zone -= 1

    return zone


# Function to perform the inference of the petrained keras model 
def model_inference(batch):

    prediction = model.predict(batch)

    # Gesture predicted from the current frame
    gesture_pre = gesture_predicted(prediction)

    # Check that it meets the correspondence interval
    # of the same gesture in 7 consecutive frames to be considered as a detected gesture
    gesture_det = [gestures_list[gest_pre] for gest_pre in gesture_pre]

    return gesture_det


# Save frame with its predicted class
def save_images(imag, gesture_det, archive_name):

    font = cv2.FONT_HERSHEY_COMPLEX

    imag = cv2.imread(imag)
    cv2.putText(imag, "Gesto: " + str(
        gesture_det), (40, 30), font, fontScale=1, color=(255, 0, 0), thickness=2)
    cv2.imwrite(path_save_images + archive_name + str(i) + ".jpg", imag)


if __name__ == "__main__":

    # Load model
    model = load_pretrained_model()

    # Counter to count the number of pictures in the current class 
    class_frames = 0

    # Count the number of images classificated in each class to compute the error with the  
    # number of pictures in the current class
    class_detected = [0, 0, 0, 0, 0]


    # Test with go gesture frames
    frames = gesture_load("go")

    # Inference with the pretrained model to compute the outputs for the batch of images <frames> as inputs 
    gesture_det = model_inference(frames)

    i = 0

    for imag in go:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[0]: # Error detected, check the class predicted in the error
            
            if gesture_det[i] == gestures_list[1]:
                class_detected[1] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[0] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="go_")
        i += 1

    # Save in a row inside the pandas dataframe
    data = data.append({' ': gestures_list[0], 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[0] / class_frames * 100, 4))}, ignore_index= True)

    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]


    # Test with stop gesture frames
    frames = gesture_load("stop")

    # Test with stop gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in stop:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[1]: # Error detected, check the class predicted in the error
            
            if gesture_det[i] == gestures_list[0]:
                class_detected[0] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[1] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="stop_")
        i += 1

    # Save in a row inside the pandas dataframe
    data = data.append({' ': gestures_list[1], 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[1] / class_frames * 100, 4))}, ignore_index= True)

    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]


    # Test with more_speed gesture frames
    frames = gesture_load("more_speed")

    # Test with more_speed gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in more_speed: 

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[2]: # Error detected, check the class predicted in the error
            
            if gestures_det[i] == gestures_list[0]:
                class_detected[0] += 1
            
            elif gesture_det[i] == gestures_list[1]:
                class_detected[1] += 1

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1            

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[2] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="more_speed_")
        i += 1

    # Save in a row inside the pandas dataframe
    data = data.append({' ': gestures_list[2], 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[2] / class_frames * 100, 4))}, ignore_index=True)

    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]


    # Test with less_speed gesture frames
    frames = gesture_load("less_speed")

    # Test with less_speed gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in less_speed:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[3]: # Error detected, check the class predicted in the error
            
            if gestures_det[i] == gestures_list[0]:
                class_detected[0] += 1
            
            elif gesture_det[i] == gestures_list[1]:
                class_detected[1] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1            

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[3] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="less_speed_")
        i += 1

    # Save in a row inside the pandas dataframe
    data = data.append({' ': gestures_list[3], 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[3] / class_frames * 100, 4))}, ignore_index=True)

    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]


    # Test with no_gesture frames
    frames = gesture_load("no_gesture")

    # Test with no gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in no_gesture:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[4]: # Error detected, check the class predicted in the error
            
            if gesture_det[i] == gestures_list[0]:
                class_detected[0] += 1
            
            elif gesture_det[i] == gestures_list[1]:
                class_detected[1] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1            

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1

        else: 
            class_detected[4] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="no_")
        i += 1

    # Save in a row inside the pandas dataframe
    data = data.append({' ': gestures_list[4], 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[4] / class_frames * 100, 4))}, ignore_index=True)

    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]

    # Print in the console the results with a more user-friendly format
    print(tabulate(data, headers='keys', tablefmt='psql'))
    
    # Save data inside the pandas dataframe in a .csv file
    data.to_csv(path_results_gestures, index=False)

    
    # Test stop gesture with right, left and both hands

    # Test with stop gesture frames
    frames = gesture_load("right")

    # Test with stop gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in stop_right:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[1]: # Error detected, check the class predicted in the error
            
            if gesture_det[i] == gestures_list[0]:
                class_detected[0] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[1] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="stop_right")
        i += 1

    # Save in a row inside the pandas dataframe
    stop_results = stop_results.append({' ': gestures_list[1] + " derecha", 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[1] / class_frames * 100, 4))}, ignore_index= True)
   
    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]
    
    # Test stop gesture with right, left and both hands

    # Test with stop gesture frames
    frames = gesture_load("left")

    # Test with stop gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in stop_left:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[1]: # Error detected, check the class predicted in the error
            
            if gesture_det[i] == gestures_list[0]:
                class_detected[0] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[1] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="stop_left")
        i += 1

    # Save in a row inside the pandas dataframe
    stop_results = stop_results.append({' ': gestures_list[1] + " izquierda", 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[1] / class_frames * 100, 4))}, ignore_index= True)

    # Reset class images counter 
    class_frames = 0
    # Reset counter of number of images per class
    class_detected = [0, 0, 0, 0, 0]

    # Test with stop gesture frames
    frames = gesture_load("both")

    # Test with stop gesture frames
    gesture_det = model_inference(frames)

    i = 0

    for imag in stop_both:

        class_frames += 1 # With each image, increment the counter
        if gesture_det[i] != gestures_list[1]: # Error detected, check the class predicted in the error
            
            if gesture_det[i] == gestures_list[0]:
                class_detected[0] += 1

            elif gesture_det[i] == gestures_list[2]:
                class_detected[2] += 1

            elif gesture_det[i] == gestures_list[3]:
                class_detected[3] += 1

            elif gesture_det[i] == gestures_list[4]:
                class_detected[4] += 1

        else: 
            class_detected[1] += 1

        # Save images with the name of the class predicted 
        save_images(imag, gesture_det[i], archive_name="stop_both")
        i += 1

    # Save in a row inside the pandas dataframe
    stop_results = stop_results.append({' ': gestures_list[1] + " ambas", 'Imagenes Clase': class_frames, gestures_list[0]: class_detected[0], gestures_list[1]: class_detected[1],
                        gestures_list[2]: class_detected[2], gestures_list[3]: class_detected[3], gestures_list[4]: class_detected[4],
                        'Precision (%)': str(round(class_detected[1] / class_frames * 100, 4))}, ignore_index= True)

    # Print in the console the results with a more user-friendly format
    print(tabulate(stop_results, headers='keys', tablefmt='psql'))
    
    # Save data inside the pandas dataframe in a .csv file
    data.to_csv(path_results_stop, index=False)