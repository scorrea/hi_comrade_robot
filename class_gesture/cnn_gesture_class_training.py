##########################################################################################
# THIS CODE WAS MADE BY: Sergio González Correa
# DATE: Lun, 6 Jun. 2020
# EMAIL: sergiogonzalezcorrea@gmail.com
##########################################################################################


import numpy as np
import matplotlib.pyplot as plt
import cv2

from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.layers import Input, Flatten, Dense, Dropout
from keras.models import Model
from keras.optimizers import Adam
import glob
import tensorflow as tf
import pandas as pd

import time

filepath_model = "/home/sergiocorrea/tfg_code/classificator_data/classificator.h5"

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Restrict TensorFlow to only use the fourth GPU
        tf.config.experimental.set_visible_devices(gpus[0], 'GPU')

        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)


# Load vgg16 model with imagenet pretrainet weights
def pretrained_model():

    # Load vgg16 withoout fully connected layers
    model_vgg16_conv = VGG16(
        weights='imagenet', include_top=False, input_shape=(244, 244, 3))
    model_vgg16_conv.summary()

    # Make convolutional layers not trainable
    for layer in model_vgg16_conv.layers:
        layer.trainable = False

    # Create your own input format
    keras_input = Input(shape=(244, 244, 3), name='image')

    # Use the generated model
    output_vgg16_conv = model_vgg16_conv(keras_input)

    # Add the fully-connected layers
    x = Flatten(name='flatten')(output_vgg16_conv)
    x = Dense(256, activation='relu', name='fc1')(x)
    # x = Dropout(0.3)(x)  # dropout to minimize overfitting
    x = Dense(128, activation='relu', name='fc2')(x)
    #x = Dropout(0.3)(x)
    x = Dense(128, activation='relu', name='fc3')(x)
    #x = Dropout(0.2)(x)
    x = Dense(64, activation='relu', name='fc4')(x)
    #x = Dropout(0.2)(x)
    x = Dense(5, activation='softmax', name='predictions')(x)

    # Create your own model
    pretrained_model = Model(inputs=keras_input, outputs=x)
    learning_rate = 1e-4
    optimizer = Adam(learning_rate=learning_rate)
    pretrained_model.compile(
        loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    return pretrained_model


def load_dataset():
    go_train = glob.glob('/home/sergiocorrea/data/train/go/*.*')
    stop_train = glob.glob('/home/sergiocorrea/data/train/stop/*.*')
    more_speed_train = glob.glob(
        '/home/sergiocorrea/data/train/more_speed/*.*')
    less_speed_train = glob.glob(
        '/home/sergiocorrea/data/train/less_speed/*.*')
    no_train = glob.glob('/home/sergiocorrea/data/train/no/*.*')

    go_test = glob.glob('/home/sergiocorrea/data/test/go_*')
    stop_test = glob.glob('/home/sergiocorrea/data/test/stop_*')
    more_speed_test = glob.glob('/home/sergiocorrea/data/test/more_speed_*')
    less_speed_test = glob.glob('/home/sergiocorrea/data/test/less_speed_*')
    no_test = glob.glob('/home/sergiocorrea/data/test/no_*')

    # Creating an empty Dataframe with column names only
    files_train = pd.DataFrame(columns=['Label', 'Path'])

    files_test = pd.DataFrame(columns=['Label', 'Path'])

    # Append rows in Empty Dataframe by adding dictionaries
    for i in go_train:
        files_train = files_train.append(
            {'Label': str(1), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_train, sep='\n')
    for i in go_test:
        files_test = files_test.append(
            {'Label': str(1), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_test, sep='\n')

    for i in stop_train:
        files_train = files_train.append(
            {'Label': str(2), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_train, sep='\n')
    for i in stop_test:
        files_test = files_test.append(
            {'Label': str(2), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_test, sep='\n')

    for i in more_speed_train:
        files_train = files_train.append(
            {'Label': str(3), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_train, sep='\n')
    for i in more_speed_test:
        files_test = files_test.append(
            {'Label': str(3), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_test, sep='\n')

    for i in less_speed_train:
        files_train = files_train.append(
            {'Label': str(4), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_train, sep='\n')
    for i in less_speed_test:
        files_test = files_test.append(
            {'Label': str(4), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_test, sep='\n')

    for i in no_train:
        files_train = files_train.append(
            {'Label': str(5), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_train, sep='\n')
    for i in no_test:
        files_test = files_test.append(
            {'Label': str(5), 'Path': i}, ignore_index=True)
    print("Dataframe Contens ", files_test, sep='\n')

    from keras.preprocessing.image import ImageDataGenerator

    datagen = ImageDataGenerator(
        preprocessing_function=preprocess_input, rescale=1./255., validation_split=0.15, horizontal_flip=True, brightness_range=[0.2, 1.0])
    testgen = ImageDataGenerator(
        preprocessing_function=preprocess_input, rescale=1./255.)

    train_generator = datagen.flow_from_dataframe(dataframe=files_train, directory=None, x_col='Path', y_col='Label', subset='training', batch_size=16, seed=42, shuffle=True,
                                                  class_mode='categorical', target_size=(244, 244), color_mode='rgb')

    valid_generator = datagen.flow_from_dataframe(dataframe=files_train, directory=None, x_col='Path', y_col='Label', subset="validation", batch_size=16, seed=42, shuffle=True,
                                                  class_mode='categorical', target_size=(244, 244), color_mode='rgb')

    test_generator = testgen.flow_from_dataframe(dataframe=files_test, directory=None, x_col='Path', y_col='Label', batch_size=16, seed=42, shuffle=False,
                                                 class_mode='categorical', target_size=(244, 244), color_mode="rgb")

    print(train_generator)
    print(valid_generator)
    print(test_generator)

    return train_generator, valid_generator, test_generator


# Function to train the model
def train_model(model, train_generator, valid_generator, test_generator):

    STEP_SIZE_TRAIN = 200  # train_generator.n // train_generator.batch_size
    STEP_SIZE_VALID = valid_generator.n // valid_generator.batch_size
    STEP_SIZE_TEST = test_generator.n // test_generator.batch_size

    # serialize model structure to JSON
    model_json = model.to_json()
    with open("/home/sergiocorrea/tfg_code/classificator_data/model.json", "w") as json_file:
        json_file.write(model_json)

    history = model.fit_generator(generator=train_generator,
                                  steps_per_epoch=STEP_SIZE_TRAIN,
                                  validation_data=valid_generator,
                                  validation_steps=STEP_SIZE_VALID,
                                  epochs=50)
    # con 40  y sinb dropout parece adaptarse mejor al validation set

    plot_gradient(history)

    score = model.evaluate_generator(
        generator=test_generator, steps=STEP_SIZE_TEST)

    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

    model.save(filepath_model)


def plot_gradient(history):
    plt.title('Model Accuaracy')
    plt.plot(history.history['accuracy'])
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'])
    plt.show()
    plt.title('Model Loss')
    plt.plot(history.history['loss'])
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'])
    plt.show()


if __name__ == "__main__":
    starting_time = time.time()
    model = pretrained_model()
    print(model.summary())
    train_generator, valid_generator, test_generator = load_dataset()
    train_model(model, train_generator, valid_generator, test_generator)
    print("Training time: " + str(time.time() - starting_time))
